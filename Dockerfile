FROM adoptopenjdk/openjdk11

COPY target/midi-tech-cluster-0.0.1-SNAPSHOT.jar /app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "app.jar"]