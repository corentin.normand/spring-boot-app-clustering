package com.liksi.miditechcluster.api

import com.liksi.miditechcluster.exception.NoBrewerAvailable
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice

@ControllerAdvice
class ExceptionHandler {

    companion object {
        private val LOGGER = LoggerFactory.getLogger(ExceptionHandler::class.java)
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(NoBrewerAvailable::class)
    fun noBrewer(ex: Exception): ResponseEntity<ErrorBean> {
        return ResponseEntity(ErrorBean("No brewer found"), HttpStatus.NOT_FOUND)
    }

}


data class ErrorBean(val message: String)
