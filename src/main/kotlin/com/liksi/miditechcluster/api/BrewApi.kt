package com.liksi.miditechcluster.api

import com.liksi.miditechcluster.exception.NoBrewerAvailable
import com.liksi.miditechcluster.model.Beer
import com.liksi.miditechcluster.model.Ingredient
import com.liksi.miditechcluster.service.IBrewingService
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
class BrewApi(private val brewingService: IBrewingService) {

    private val logger = KotlinLogging.logger {}

    @PostMapping("brew", consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.OK)
    fun brewBeer(@RequestBody ingredients: List<Ingredient>): Beer {

        val brewer = brewingService.pickABrewer() ?: throw NoBrewerAvailable()
        ingredients.map {
            logger.info { "Handling ingredient $it" }
            Thread.sleep(2000)
        }
        brewingService.putBrewerInPool(brewer)
        return Beer(
            label = "Sant Erwann IPA",
            alcoolRate = 8.0,
            brewery = "Brasserie de Bretagne",
            style = "IPA",
            brewer = brewer.name
        )
    }
}
