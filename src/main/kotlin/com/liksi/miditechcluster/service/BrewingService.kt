package com.liksi.miditechcluster.service

import com.liksi.miditechcluster.model.Brewer
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import com.liksi.miditechcluster.service.brewing.IBrewerProvider
import org.springframework.stereotype.Service
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock
import javax.annotation.PostConstruct
import kotlin.concurrent.withLock

@ConditionalOnProperty(name = ["useHazelcast"], havingValue = "false")
@Service
class BrewingService(brewers: IBrewerProvider) : IBrewingService {

    // Fetch brewers from the provider (could be an external call)
    private val allBrewers = brewers.listBrewers()

    // The map of all brewers
    private val freeBrewers: MutableMap<String, Brewer> = mutableMapOf()

    private val lock: Lock = ReentrantLock()

    @PostConstruct
    fun init() {
        allBrewers.forEach {
            if (!freeBrewers.containsKey(it.name)) {
                freeBrewers[it.name] = it
            }
        }
    }

    override fun pickABrewer(): Brewer? {
        lock.withLock {
            val result = freeBrewers.values.firstOrNull { !it.busy  }
            result?.let {
                result.busy = true
            }
            return result
        }
    }

    override fun putBrewerInPool(brewer: Brewer) {
        brewer.busy = false
        freeBrewers[brewer.name] = brewer
    }

}
