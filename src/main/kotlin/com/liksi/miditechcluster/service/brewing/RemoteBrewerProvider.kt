package com.liksi.miditechcluster.service.brewing

import com.liksi.miditechcluster.model.Brewer
import org.springframework.stereotype.Service

@Service
class RemoteBrewerProvider : IBrewerProvider {
    override fun listBrewers(): List<Brewer> {
        return listOf(Brewer("Maryvonne"), Brewer("Jean-Pierre"))
    }


}
