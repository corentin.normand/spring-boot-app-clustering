package com.liksi.miditechcluster.service.brewing

import com.liksi.miditechcluster.model.Brewer

interface IBrewerProvider {

    fun listBrewers() : List<Brewer>

}
