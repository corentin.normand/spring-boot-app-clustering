package com.liksi.miditechcluster.service.stocks

import mu.KotlinLogging
import org.springframework.stereotype.Service

@Service
class StockCheckService {
    private val logger = KotlinLogging.logger {}


    fun checkStocks() {
        logger.info { "Checking Stocks..." }
        Thread.sleep(10_000)
        logger.info { "Everything is ok" }
    }
}