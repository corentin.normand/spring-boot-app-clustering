package com.liksi.miditechcluster.service.scheduler

import com.liksi.miditechcluster.service.stocks.StockCheckService
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Profile("spring")
@Service
class SpringSchedulerImpl(
    private val stockCheckService: StockCheckService,
) {

    @Scheduled(cron = "\${tasks.check-stores}")
    fun scheduleStoreChecks() {
        stockCheckService.checkStocks()
    }
}