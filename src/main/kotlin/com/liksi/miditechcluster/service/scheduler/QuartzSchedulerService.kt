package com.liksi.miditechcluster.service.scheduler

import com.liksi.miditechcluster.service.stocks.StockCheckService
import org.quartz.Job
import org.quartz.JobExecutionContext
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service


@Profile("quartz")
@Service
class QuartzSchedulerService(private val stockCheckService: StockCheckService) : Job {
    /**
     * Le JobExecutionContext contient des métadonnées propres au Job exécuté :
     *  - dernière exécution
     *  - prochaine exécution
     *  - informations sur le Trigger, jobDetail, Scheduler, etc.
     */
    override fun execute(jobExecutionContext: JobExecutionContext) {
        stockCheckService.checkStocks()
    }
}