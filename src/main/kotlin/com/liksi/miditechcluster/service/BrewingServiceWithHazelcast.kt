package com.liksi.miditechcluster.service

import com.hazelcast.core.HazelcastInstance
import com.hazelcast.cp.lock.FencedLock
import com.hazelcast.map.IMap
import com.liksi.miditechcluster.model.Brewer
import com.liksi.miditechcluster.service.brewing.IBrewerProvider
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct
import kotlin.concurrent.withLock

@ConditionalOnProperty(name = ["useHazelcast"], havingValue = "true")
@Service
class BrewingServiceWithHazelcast(brewers : IBrewerProvider, @Qualifier("hazelcastInstance") val hazelcast: HazelcastInstance) : IBrewingService {

    private val allBrewers = brewers.listBrewers()

    private val freeBrewers : IMap<String, Brewer> = hazelcast.getMap("brewers")

    private fun getLock(): FencedLock {
        return hazelcast.cpSubsystem.getLock("brewers")
    }

    @PostConstruct
    fun init() {
        getLock().withLock {
            allBrewers.forEach {
                if (!freeBrewers.containsKey(it.name)) {
                    freeBrewers[it.name] = it
                }
            }
        }
    }

    override fun pickABrewer() : Brewer? {
        getLock().withLock {
            val result = freeBrewers.values.firstOrNull { !it.busy  }
            result?.let {
                result.busy = true
                freeBrewers[result.name] = result
            }
            return result
        }
    }

    override fun putBrewerInPool(brewer : Brewer) {
        brewer.busy = false
        freeBrewers[brewer.name] = brewer
    }

}
