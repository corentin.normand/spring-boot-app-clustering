package com.liksi.miditechcluster.service

import com.liksi.miditechcluster.model.Brewer

interface IBrewingService {
    fun pickABrewer() : Brewer?
    fun putBrewerInPool(brewer: Brewer)

}
