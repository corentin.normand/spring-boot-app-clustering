package com.liksi.miditechcluster.model

enum class Ingredient {
    EAU,HOUBLON,MALT,ORGE,MAIS,RIZ,LEVURE
}