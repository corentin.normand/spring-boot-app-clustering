package com.liksi.miditechcluster.model

import java.io.Serializable

data class Brewer(val name: String, var busy: Boolean = false) : Serializable
