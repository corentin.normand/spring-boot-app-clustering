package com.liksi.miditechcluster.model

data class Beer(
    val label: String? = null,
    val alcoolRate: Double? = null,
    val brewery: String? = null,
    val style: String? = null,
    val brewer: String
)
