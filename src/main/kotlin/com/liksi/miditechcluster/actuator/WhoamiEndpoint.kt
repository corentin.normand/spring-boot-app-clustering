package com.liksi.miditechcluster.actuator

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.actuate.endpoint.annotation.Endpoint
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation
import org.springframework.stereotype.Component

@Component
@Endpoint(id = "whoami")
class WhoamiEndpoint(
    @Value("\${app.name}") private val name: String
) {

    @ReadOperation
    fun getName() = name
}