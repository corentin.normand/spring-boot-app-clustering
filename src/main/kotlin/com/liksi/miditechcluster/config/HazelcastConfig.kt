package com.liksi.miditechcluster.config

import com.hazelcast.config.Config
import com.hazelcast.config.ListConfig
import com.hazelcast.config.MapConfig
import com.hazelcast.core.Hazelcast
import com.hazelcast.core.HazelcastInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@ConditionalOnProperty(name = ["useHazelcast"], havingValue = "true")
@Configuration
class HazelcastConfig {

    @Value("\${instanceName}")
    private lateinit var name: String

    @Value("\${cpMemberCount:0}")
    private lateinit var cpMemberCount: String

    @Bean
    fun hazelcastInstance(): HazelcastInstance {
        val config = Config()
        config.addMapConfig(
            MapConfig()
                .setName("brewers")
                .setBackupCount(1)
                .setAsyncBackupCount(0)
                .setReadBackupData(false)
        )
        config.cpSubsystemConfig.cpMemberCount = cpMemberCount.toInt()
        config.instanceName = name
        return Hazelcast.newHazelcastInstance(config)
    }

}
