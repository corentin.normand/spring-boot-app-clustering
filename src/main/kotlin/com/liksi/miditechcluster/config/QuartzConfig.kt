package com.liksi.miditechcluster.config

import com.liksi.miditechcluster.service.scheduler.QuartzSchedulerService
import org.quartz.CronScheduleBuilder.cronSchedule
import org.quartz.JobDetail
import org.quartz.Trigger
import org.quartz.TriggerBuilder
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.quartz.JobDetailFactoryBean


@Configuration(proxyBeanMethods = false)
@Profile("quartz")
class QuartzConfig(@Value("\${tasks.check-stores}") private val cronCheckStores: String) {

    /**
     * Configuration du JOB à exécuter, dans notre cas la vérification des Stocks
     */
    @Bean
    fun jobDetail(): JobDetailFactoryBean {
        val jobDetailFactory = JobDetailFactoryBean()
        jobDetailFactory.setJobClass(QuartzSchedulerService::class.java)
        jobDetailFactory.setGroup("group")
        jobDetailFactory.setDescription("Invoke check stock")
        jobDetailFactory.setDurability(true)
        return jobDetailFactory
    }

    /**
     * Configuration de scheduling pour un job donné avec une expression CRON
     */
    @Bean
    fun trigger(job: JobDetail): Trigger {
        return TriggerBuilder.newTrigger()
            .forJob(job)
            .withIdentity("Qrtz_Trigger")
            .withDescription("Sample trigger")
            .withSchedule(cronSchedule(cronCheckStores))
            .build()
    }
}